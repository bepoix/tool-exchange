# Tool exchange for Fusion & DaVinciResolve

### Overview
![toolXchange GUI](screentoolsxchange.JPG)

### Note
This repo, isn't updated, but you can find an updated version from movalex :
https://github.com/movalex/fusion/blob/master/Scripts/Comp/ToolXChange.lua


### How it works

This script is a workaround of not being able to launch a bins servers 
for sharing tools, settings and other over a network.

At the first start it will ask you a repository, enter a path that will not change.
Repository store all publish's user with JSON format.

### Install

*copy this lua script to :*
* **Windows**:
```bash
%appdata%/Blackmagic Design/Fusion/Scripts/Comp/
```

* **MacOS**:
```bash
$HOME/Library/Application Support/Blackmagic Design/Fusion/Scripts/Comp/
```

* **Linux**:
```bash
$HOME/.fusion/BlackmagicDesign/Fusion/Scripts/Comp/
```
